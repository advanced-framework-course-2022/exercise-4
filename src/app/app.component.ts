import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

interface Pokemon {
  name: string;
  url: string;
}

interface PokemonList {
  count: number;
  next?: string;
  previous?: string;
  results: Pokemon[];
}

const POKEMON_API_URL = 'https://pokeapi.co/api/v2/pokemon';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  public pokemonList?: PokemonList;
  
  private subscription?: Subscription;

  constructor(private readonly httpClient: HttpClient) {}

  ngOnInit(): void {
    this.subscription = this.pokemonList$.subscribe(response => this.pokemonList = response);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private get pokemonList$(): Observable<PokemonList> {
    return this.httpClient.get<PokemonList>(POKEMON_API_URL);
  }

}
